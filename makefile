#MakeFile

#make all
all:
	 gcc -o queue_test queue.c queue.h testafila.c -w

#make run
run:
	 make && ./queue_test && clean

#make clean
clean: 
	rm *.o
