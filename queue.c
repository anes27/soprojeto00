#include "queue.h"
#include <stdio.h>
#include <stdlib.h>

void queue_append (queue_t **queue, queue_t *elem){
	if((*queue) == NULL ){
		if(elem->next == NULL && elem->prev ==NULL){
		(*queue) = elem;
		(*queue)->next = elem;
		(*queue)->prev = elem;
	}
	}else{
		if(elem->next == NULL && elem->prev ==NULL){
		elem->next = (*queue);
		elem->prev = (*queue)->prev;
		(*queue)->prev->next = elem;
		(*queue)->prev = elem;
		}
	}
}

queue_t *queue_remove (queue_t **queue, queue_t *elem){
	queue_t *aux;
	int tam = queue_size((*queue));
	int count = 0;
	if(queue != NULL && elem != NULL){
		aux = (*queue);
			do{
				if(aux == elem){
					if(aux == aux->prev && aux == aux->next){
						aux->next = NULL;
						aux->prev = NULL;
						(*queue) = NULL;
						return elem;
					}
					if(aux == (*queue)){
						(*queue) = (*queue)->next;
					}
					aux->prev->next = aux->next;
					aux->next->prev = aux->prev;
					aux->next = NULL;
					aux->prev = NULL;
					return elem;
				}
				aux = aux->next;
				count++;
			}while(tam != count);
			return NULL;
		}	
		return NULL;
	}
	

 int queue_size (queue_t *queue){
	 queue_t *chave;
     
     int contador = 0;
	
     if(queue != NULL){
		 
         chave = queue;
   
		 do{
			 contador++;
			 chave = chave->next;
         }while(chave != queue);
		 
         return(contador);
		
	}else{
         return 0;
    }
 }

void queue_print (char *name, queue_t *queue, void print_elem (void*)){
	queue_t *aux;
	queue_t *chave;
	aux = queue;
	chave = queue;
	if(queue != NULL){
		printf("%s: [", name);
		do{
			print_elem(aux);
			aux = aux->next;
			if(chave != aux){
				printf(" ");
			}
		}while(chave != aux);
		printf("]\n");
	}
}
